﻿
$(document).ready(function () {

    /**Metodo para recuperar oficinas*/
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44312/api/salary/getoffices', //llamada a la ruta
        success: function (data) {

            var datos = '<option selected disabled value ="">Seleccione</option>';
            data.forEach(element => {
                datos += '<option  value="' + element.id + '">' + element.name + '</option>';
            });

            $('#oficina_empleado').append(datos);
        },
        error: function (err) {
            alert(err.responseText);
            console.log(err.responseText);
        }

    });

    /**Metodo para recuperar Posiciones*/
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44312/api/salary/getpositios', //llamada a la ruta
        success: function (data) {

            var datos = '<option selected disabled value ="">Seleccione</option>';
            data.forEach(element => {
                datos += '<option  value="' + element.id + '">' + element.name + '</option>';
            });

            $('#posicion_empleado').append(datos);
        },
        error: function (err) {
            alert(err.responseText);
            console.log(err.responseText);
        }

    });

    /**Metodo para recuperar Divisiones*/
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44312/api/salary/getdivisios', //llamada a la ruta
        success: function (data) {

            var datos = '<option selected disabled value ="">Seleccione</option>';
            data.forEach(element => {
                datos += '<option  value="' + element.id + '">' + element.name + '</option>';
            });

            $('#division_empleado').append(datos);
        },
        error: function (err) {
            alert(err.responseText);
            console.log(err.responseText);
        }

    });

    /**Recupera encabezado*/
    $('#codigo_empleado').blur(function () {
        let employeeCode = this.value
        $.ajax({
            type: 'GET',
            url: 'https://localhost:44312/api/salary/' + employeeCode, //llamada a la ruta
            success: function (data) {

                if (data) {
                    $('#codigo_empleado').val(data.employeeCode);
                    $('#fecha_empleado').val(data.birthdays);
                    $('#fecha_inicio_empleado').val(data.beginDates);
                    $('#identificacion_empleado').val(data.identificationNumber);
                    $('#nombres_empleado').val(data.employeeName);
                    $('#apellidos_empleado').val(data.employeeSurname);
                    $('#division_empleado').val(data.divisionId);
                    $('#oficina_empleado').val(data.officeId);
                    $('#posicion_empleado').val(data.positionId);
                    $('#grado_empleado').val(data.grade);
                    resetForm();
                    getsalaryDetail(employeeCode);
                } else {
                    $('#fecha_inicio_empleado,#fecha_empleado,#identificacion_empleado,#nombres_empleado,#apellidos_empleado,#division_empleado,#oficina_empleado,#posicion_empleado,#grado_empleado').val('');
                    resetForm()
                }
                
            },
            error: function (err) {
                alert(err.responseText);
                console.log(err.responseText);
            }

        });
    });

    /**Metodo para añadir fila a tabla Detalle servicio*/
    $("#table_empleados").on('click', '.adicionarFila', function () {
        adicionarFila()
    });


    /** Funcion para eliminar fila */
    $("#table_empleados").on('click', '.eliminarFila', function () {

        var numeroFilas = $("#table_empleados tr").length;
        if (numeroFilas > 2) {
            $(this).closest('tr').remove();
        } else {
            alert("¡Esta fila no puede ser eliminada!");
        }

    });

    /** Limpia el formulario */
    function resetForm() {
        libros = [];
        /** Elimina todas las filas de tabla dinamica menos la primera */
        $('#tbodytableSalario tr').closest('.otrasFilas').remove();
        $("#tbodytableSalario tr").find("input").val('');
    }

    function getsalaryDetail(employeeCode) {
        $.ajax({
            type: 'GET',
            url: 'https://localhost:44312/api/salary/detail/' + employeeCode, //llamada a la ruta
            success: function (response) {
                response.forEach(cargartablaDetalle);
            },
            error: function (err) {
                console.log(err.responseText);
                Alert("¡Ha ocurrido un error inesperado!");
            }
        });

    }

    /**Metodo para añadir fila a tabla detalle */
    function adicionarFila() {
        $("#table_empleados tbody tr:eq(0)").clone().addClass("otrasFilas").removeClass("fila-base").appendTo("#table_empleados tbody").find("input").val("");
    }

    function cargartablaDetalle(item, index) {

        if (0 == index) {
            $("#txtid").val(item['id']);
            $("#txtAnio").val(item['year']);
            $("#txtMes").val(item['month']);
            $("#txtsalario").val(item['salary']);
            $("#txtBonoPro").val(item['productionBonus']);
            $("#txtBonoComp").val(item['compensationBonus']);
            $("#txtComision").val(item['commission']);
            $("#txtContri").val(item['contributions']);

        } else {
            adicionarFila();
            $("#table_empleados tbody tr:eq(" + index + ")").find("input").each(function () {

                if ($(this).attr("id") == "txtid")
                    $(this).val(item['id']);
                if ($(this).attr("id") == "txtAnio")
                    $(this).val(item['year']);
                if ($(this).attr("id") == "txtMes")
                    $(this).val(item['month']);
                if ($(this).attr("id") == "txtsalario")
                    $(this).val(item['salary']);
                if ($(this).attr("id") == "txtBonoPro")
                    $(this).val(item['productionBonus']);
                if ($(this).attr("id") == "txtBonoComp")
                    $(this).val(item['compensationBonus']);
                if ($(this).attr("id") == "txtComision")
                    $(this).val(item['commission']);
                if ($(this).attr("id") == "txtContri")
                    $(this).val(item['contributions']);

            });
        }
    }

    $('#btnsave').click(function () {
        
        var DATA = [];
        var TABLA = $("#table_empleados tbody > tr");
        let control = true;
        /**Dato que no se repite */
        var codigo_emplead = $('#codigo_empleado').val();
        var fecha_empleado = $('#fecha_empleado').val();
        var fecha_inicio_empleado = $('#fecha_inicio_empleado').val();
        var identificacion_empleado = $('#identificacion_empleado').val();
        var nombres_empleado = $('#nombres_empleado').val();
        var apellidos_empleado = $('#apellidos_empleado').val();
        var division_empleado = $('#division_empleado').val();
        var oficina_empleado =$('#oficina_empleado').val();
        var posicion_empleado = $('#posicion_empleado').val();
        var grado_empleado = $('#grado_empleado').val();

        if (codigo_emplead == '' || fecha_empleado == '' || fecha_inicio_empleado == '' || identificacion_empleado == ''
            || nombres_empleado == '' || apellidos_empleado == '' || division_empleado == '' || posicion_empleado == '' || grado_empleado == '') {

            alert('Favor Completar Datos del empleado');
            return false;

        }

        /*Obtención de datos de la tabla dinámica*/
        TABLA.each(function (e) {

           
            /**Datos dinamicos */
            let id = $(this).find("input[id*='txtid']").val();
            let anio = $(this).find("input[id*='txtAnio']").val();
            let mes = $(this).find("input[id*='txtMes']").val();

            DATA.forEach(object => {
                if (object.Year === parseInt(anio) && object.Month === parseInt(mes)) {

                    alert('No Es Posible Guardar 2 Registros en el mes ' + mes + ' del  año ' + anio)
                    control = false;
                }
            });
            

            let salario = $(this).find("input[id*='txtsalario']").val();
            let BonoPro = $(this).find("input[id*='txtBonoPro']").val();
            let BonoComp = $(this).find("input[id*='txtBonoComp']").val();
            let Comision = $(this).find("input[id*='txtComision']").val();
            let contribucion = $(this).find("input[id*='txtContri']").val();

                item = {};
            /**Datos dinamicos */
            if (id == null || id==undefined ||  id=='')
                id = 0;

                item["Id"] = parseInt(id);
                item["Year"] = parseInt(anio);
                item["Month"] = parseInt(mes);
                item["BaseSalary"] = parseFloat(salario);
                item["ProductionBonus"] = parseFloat(BonoPro);
                item["CompensationBonus"] = parseFloat(BonoComp);
                item["Commission"] = parseFloat(Comision);
                item["Contributions"] = parseFloat(contribucion);

                /**Dato que no se repite */
                item["EmployeeCode"] = codigo_emplead;
                item["Birthday"] = fecha_empleado;
                item["BeginDate"] = fecha_inicio_empleado;
                item["IdentificationNumber"] = identificacion_empleado;
                item["EmployeeName"] = nombres_empleado;
                item["EmployeeSurname"] = apellidos_empleado;
                item["DivisionId"] = parseInt(division_empleado);
                item["OfficeId"] = parseInt(oficina_empleado);
                item["PositionId"] = parseInt(posicion_empleado);
                item["Grade"] = parseInt(grado_empleado); ;

            DATA.push(item);

            if (anio == '' || mes == '' || salario == '' || BonoPro == '' || BonoComp == '' || Comision == '' || contribucion == '' ) {
                alert('No es posible Guardar Datos vacios, revisar informacion');
                control = false;
            }
            if (!control)
                return false;


        });
        if (!control)
            return false;
        let detalle = JSON.stringify(DATA);

        $.ajax({
            type: 'POST',
            url: 'https://localhost:44312/api/salary/save', //llamada a la ruta
            data: detalle,
            contentType: "application/json",
            success: function (response) {
                alert(response)
                $('#fecha_inicio_empleado,#fecha_empleado,#identificacion_empleado,#nombres_empleado,#apellidos_empleado,#division_empleado,#oficina_empleado,#posicion_empleado,#grado_empleado').val('');
                resetForm()

            },
            error: function (err) {
                console.log(err.responseText);
                alert("¡Ha ocurrido un error inesperado!");
            }
        });

    });

});