﻿var table = null;

$(document).ready(function () {



    $('#btnBuscar').click(function () {
        getEmployee();
    });

    function getEmployee (){

        var EmployeeCode = $('#fin-empleado').val();

        if (EmployeeCode != '' || EmployeeCode != null) {
            $.ajax({
                type: 'GET',
                url: 'https://localhost:44312/api/salary/bono/' + EmployeeCode, //llamada a la ruta
                success: function (data) {

                    if (data) {
                        $('#nombre_empleado').val(data.employeeCompleteName);
                        $('#division_empleado').val(data.division);
                        $('#posicion_empleado').val(data.position);
                        getsalaryEmployee(EmployeeCode)
                    } else {
                        $('#txtbono').text('');
                        alert("No se encontro registro");
                        $("#table_empleados").DataTable().destroy();
                        $('#table_empleados tbody').empty();
                        $('#nombre_empleado,#division_empleado,#posicion_empleado').val('');
                    }
                        
                },
                error: function (err) {
                    alert('Ocurrio un error');
                    console.log(err.responseText);
                }

            });
        }
    }

    function getsalaryEmployee(EmployeeCode) {

        $("#table_empleados").DataTable().destroy();

        table = $('#table_empleados').DataTable({
            processing: true,
            ajax: {
                type: "GET",
                url: 'https://localhost:44312/api/salary/bono/salary/'+EmployeeCode,
                dataSrc: ""
            },
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ning&uacute;n dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "&Uacute;ltimo",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            columns: [
                { "data": "year" },
                { "data": "month" },
                { "data": "beginDates" },
                { "data": "salary" }
            ]
        });

        setTimeout(function () {
            findbono();
        }, 1200);
        
    }

    function findbono()
    {
        var data = table.rows().data();
        var indexultimo = data.length;
        indexultimo = indexultimo-1
        var b = false;
        var c = false;
        var bono = data[indexultimo].salary;

        if (data[indexultimo].month - data[indexultimo - 1].month == 1)
        {
            bono += data[indexultimo - 1].salary
            b = true
        }

        if (data[indexultimo-1].month - data[indexultimo - 2].month == 1)
            if (b) {
                bono += data[indexultimo - 2].salary
                c = true
            }

        $('#table_empleados tbody tr').each(function (index) {

            if (index == indexultimo)
              $(this).addClass('selected');
            if (index == indexultimo-1 && b==true)
                $(this).addClass('selected');
            if (index == indexultimo-2 && c==true)
                $(this).addClass('selected');
    
        });

        bono = bono / 3
        alert('El Bono del Empleado es ' + bono);
        $('#txtbono').text('El Bono del Empleado es ' + bono);
    }

});