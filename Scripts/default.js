﻿
$(document).ready(function () {

    var employeeCode = null;

    var table =$('#table_empleados').DataTable({
        processing: true,
        ajax: {
            type: "GET",
            url: 'https://localhost:44312/api/salary',
            dataSrc: ""

        },
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ning&uacute;n dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "&Uacute;ltimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        columns: [
            { "data": "employeeCode" },
            { "data": "employeeCompleteName" },
            { "data": "division" },
            { "data": "position" },
            { "data": "grade" },
            { "data": "beginDates" },
            { "data": "birthdays" },
            { "data": "identificationNumber" },
            { "data": "totalSalary" }
        ]
    });

    $('#table_empleados tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            employeeCode = null;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var data = table.row(this).data();
            employeeCode = data.employeeCode
        }
    });


    $('#btnOficinaGrado').click(function () {
        if (employeeCode != null)
            getfiltro(employeeCode, 1);
    });
    $('#btnOficinasGrado').click(function () {
        if (employeeCode != null)
            getfiltro(employeeCode, 2);
    });
    $('#btnPosicionGrado').click(function () {
        if (employeeCode != null)
            getfiltro(employeeCode, 3);
    });
    $('#btnGradoIngreso').click(function () {
        if (employeeCode != null)
            getfiltro(employeeCode, 4);
    });

    $('#btnMostrarTodo').click(function () {
        window.location.href = '/';
    });


 
    function getfiltro(employeeCode, filtro) {

        $("#table_empleados").DataTable().destroy();
        var table = $('#table_empleados').DataTable({
            processing: true,
            ajax: {
                type: "GET",
                url: 'https://localhost:44312/api/salary/' + employeeCode + '/' + filtro,
                dataSrc: ""

            },
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ning&uacute;n dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "&Uacute;ltimo",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            columns: [
                { "data": "employeeCode" },
                { "data": "employeeCompleteName" },
                { "data": "division" },
                { "data": "position" },
                { "data": "grade" },
                { "data": "beginDates" },
                { "data": "birthdays" },
                { "data": "identificationNumber" },
                { "data": "totalSalary" }
            ]
        });

    }

});
