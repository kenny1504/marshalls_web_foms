﻿<%@ Page Title="Empleados" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Marshalls_LLC._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    

<div class="example-wrapper m-2">
    
	<link rel="stylesheet" href="/Content/datatable.css">
	<script src="/Scripts/jquery-datatable.js" type="text/javascript"></script>
	<script src="/Scripts/datatable.js" type="text/javascript"></script>
	<script src="/Scripts/default.js" type="text/javascript"></script>

    <div class="row text-body-page">
      <div class="col-md-10 m">
          <button type="button" class="btn-sm btn-info text-white m-1" title="Filtra Empleados con la misma Oficina y Grado" id="btnOficinaGrado">Misma Oficina/Grado</button>
          <button type="button" class="btn-sm btn-info text-white m-1" title="Filtra Empleados de todas las Oficinas y con el mismo Grado" id="btnOficinasGrado">Todas Oficinas/Grado</button>
          <button type="button" class="btn-sm btn-info text-white m-1" title="Filtra Empleados con la misma Posición y Grado" id="btnPosicionGrado">Misma Posición/Grado</button>
          <button type="button" class="btn-sm btn-info text-white m-1" title="Filtra Empleados con el mismo Grado y el mismo mes de Ingreso" id="btnGradoIngreso">Mismo Grado/Ingreso</button>
          <button type="button" class="btn-sm btn-info text-white m-1" title="Mostar Todos" id="btnMostrarTodo">Mostrar Todos</button>
	 </div>
  </div>

	<div class="row pt-3 ">
		<table id="table_empleados" class="table-responsive  table table-hover table-striped">
			<thead>
				<tr>
					<th scope="col">Código Empleado</th>
					<th scope="col">Nombre Completo</th>
					<th scope="col">División</th>
					<th scope="col">Posición</th>
					<th scope="col">Grado</th>
					<th scope="col">Fecha de Inicio</th>
					<th scope="col">Cumpleaños</th>
					<th scope="col">Identificación</th>
					<th scope="col">Salario Total</th>
				</tr>
			</thead>
			<tbody id='tbodytable'>
			</tbody>
		</table>
	</div>
   
</div>
</asp:Content>
