﻿<%@ Page Title="Bono Empleado" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Bono.aspx.cs" Inherits="Marshalls_LLC.Bono" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <link rel="stylesheet" href="/Content/datatable.css">
	<script src="/Scripts/jquery-datatable.js" type="text/javascript"></script>
	<script src="/Scripts/datatable.js" type="text/javascript"></script>
    <script src="/Scripts/bono.js" type="text/javascript"></script>
    
    <div class="row  text-body-page m-1">
      <div class="col-md-12">
           <div class="col-md-2">
             <input maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" id="fin-empleado" type="text" class="no-outline form-control" title="Ingrese codigo de empleado" placeholder="Codigo Empleado">
           </div>
           <div class="col-md-2">
               <button id="btnBuscar"  type="button" class="btn-sm btn-info text-white m-1">Buscar </button>
           </div>
           <div class="col-md-6"> 
              <label id="txtbono" style="color:red"></label>
          </div>
      </div>
    </div>
    <div class="row  text-body-page m-1">
      <div class="col-md-6"> 
          <label>Nombre Completo</label>
         <input id="nombre_empleado" type="text" class="form-control" disabled>
      </div>
         <div class="col-md-3"> 
          <label>División</label>
         <input id="division_empleado" type="text" class="form-control" disabled>
      </div>
         <div class="col-md-3"> 
          <label>Posición</label>
         <input id="posicion_empleado" type="text" class="form-control" disabled>
      </div>
    </div>

    <div class="row pt-3 ">
		<table id="table_empleados" class="table table-hover text-body-page">
			<thead>
				<tr>
					<th scope="col">Año</th>
					<th scope="col">Mes</th>
					<th scope="col">Fecha Inicio</th>
					<th scope="col">Salario</th>
				</tr>
			</thead>
			<tbody id='tbodytable'>
			</tbody>
		</table>
	</div>

</asp:Content>
