﻿<%@ Page Title="Salario Empleado" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Salario.aspx.cs" Inherits="Marshalls_LLC.Salario" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<script src="/Scripts/Salario.js" type="text/javascript"></script>

    <div class="row  m-1">
		<div class="col-md-3">
			<label>Codigo Empleado</label>
			<input required maxlength="10"  minlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" id="codigo_empleado" type="text" class="form-control">
		</div>
		<div class="col-md-3">
			<label>Fecha Inicio</label>
			<input id="fecha_inicio_empleado" type="date" required class="form-control" >
		</div>
		<div class="col-md-3">
			<label>Identificación</label>
			<input maxlength="10"  id="identificacion_empleado" required type="text" class="form-control" >
		</div>
		<div class="col-md-3">
			<label>Fecha Nacimiento</label>
			<input  id="fecha_empleado" type="date" required class="form-control" >
		</div>
	</div>

	<div class="row  m-1">
		<div class="col-md-6">
			<label>Nombres</label>
			<input maxlength="150" id="nombres_empleado" type="text" required class="form-control" >
		</div>
		<div class="col-md-6">
			<label>Apellidos</label>
			<input maxlength="150" id="apellidos_empleado" type="text" required class="form-control" >
		</div>
	</div>

	<div class="row m-1">
		
		<div class="col-md-3">
			<label>División</label>
			<select class="form-control" required id="division_empleado"> </select>
		</div>
		<div class="col-md-3">
			<label>Oficina</label>
			<select class="form-control" required id="oficina_empleado"> </select>
		</div>
		<div class="col-md-3">
			<label>Posición</label>
		     <select class="form-control" required id="posicion_empleado"> </select>
		</div>
		<div class="col-md-3">
			<label>Grado</label>
			<input id="grado_empleado" type="text"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" required class="form-control" >
		</div>
	</div>

    <div class="row pt-3 ">
		<table id="table_empleados" class="table table-hover text-body-page">
			<thead>
				<tr class="bg-table">
					<th hidden scope="col">Año</th>
					<th scope="col">Año</th>
					<th scope="col">Mes</th>
					<th scope="col">Salario</th>
                    <th scope="col">Bono Productivo</th>
                    <th scope="col">Bono Compesantorio</th>
                    <th scope="col">Comision</th>
                    <th scope="col">Contribuciones</th>
					<th scope="col">Accion</th>
				</tr>
			</thead>
			
			<tbody id='tbodytableSalario'>
				<tr class="fila-base">
					<td hidden class="text-center">
						<input onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtid" id="txtid" class="text-center form-control input-sm">
					</td>
					<td class="text-center">
						<input maxlength="4"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtanio" id="txtAnio" class="text-center form-control input-sm">
					</td>
					<td class="text-center">
						<input  maxlength="2" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtmes" id="txtMes" class="text-center form-control input-sm">
					</td>
					<td class="text-center">
						<input onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtsalario" id="txtsalario" class=" text-center form-control input-sm">
					</td>
					<td class="text-center">
						<input onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtbonopro" id="txtBonoPro" class=" text-center form-control input-sm">
					</td>
					<td class="text-center">
						<input onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtBonoComp" id="txtBonoComp" class="total text-center form-control input-sm">
					</td>
					<td class="text-center">
						<input onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtComision" id="txtComision" class=" text-center form-control input-sm">
					</td>
					<td class="text-center">
						<input onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/[^0-9\.]/g, '')" type="text" name="txtContri" id="txtContri" class="total text-center form-control input-sm">
					</td>
					<td class="text-center">
						<button type="button" class="btn btn-danger eliminarFila"
							title="Eliminar registro"
							id="btnEliminarFila">
							--
						</button>
						<button type="button" class="adicionarFila btn btn-primary"
							id="btnAdicionarFila"
							title="Adicionar registro">
							+
						</button>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="row">
			<div class="col-md-3 offset-md-2">
			<br>
			<button id="btnsave" type="button" class="btn btn-success">Guardar</button>
		</div>
		</div>
	</div>

</asp:Content>
